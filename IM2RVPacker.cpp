#include "IM2RVPacker.h"
#include "LibGeo.h"
TIM2RVPacker::TIM2RVPacker(TIM2RVParams &wParams)
	: Params (wParams)
	, Bsk    (0.)
	, Lsk    (0.)
	, Hsk    (0.)
	, Bwgs   (0.)
	, Lwgs   (0.)
	, Hwgs   (0.)
	, Xwgs   (0.)
	, Ywgs   (0.)
	, Zwgs   (0.)
	, Psi    (0.)
	, Theta  (0.)
	, Gamma  (0.)
{	
	for(int i = 0; i < 3;  ++i){A[i]  = 0.;}
	for(int i = 0; i < 3;  ++i){V[i]  = 0.;}
	for(int i = 0; i < 10; ++i){Bx[i] = 0.;}
}
//---------------------------------------------------------------------------
int TIM2RVPacker::Handler(TMarshrut *pMarshrut){

	if(pMarshrut == NULL){return -1;}

	Bsk = pMarshrut->GlobStr->fi;
	Lsk = pMarshrut->GlobStr->la;
	Hsk = pMarshrut->GlobStr->h;

	for (int i = 0; i <3; ++i){V[i] = pMarshrut->Vgeo->Otn[i];}
	for (int i = 0; i <3; ++i){A[i] = pMarshrut->A.Geo[i];}

	Psi =  -pMarshrut->GlobStr->psi * 180 / M_PI;
	Theta = pMarshrut->GlobStr->teta * 180 / M_PI;
	Gamma = pMarshrut->GlobStr->gamma * 180 / M_PI;

	CalcWGSCoord();
	ReCalcBx();
	SetData();
	return 0;
}
//---------------------------------------------------------------------------
int TIM2RVPacker::Handler(CBO *pCBO){

	if(pCBO == NULL){return -1;}

	Bsk = pCBO->B;
	Lsk = pCBO->L;
	Hsk = pCBO->H;
	double Vgeo[3] = {pCBO->vg[2], pCBO->vg[0], pCBO->vg[1]}; 

	for (int i = 0; i <3; ++i){V[i] = Vgeo[i];}
	for (int i = 0; i <3; ++i){A[i] = 0.;}

	Psi =   pCBO->psi * 180 / M_PI;
	Theta = pCBO->tet * 180 / M_PI;
	Gamma = pCBO->gam * 180 / M_PI;

	CalcWGSCoord();
	ReCalcBx();
	SetData();
	return 0;
}

//---------------------------------------------------------------------------
int TIM2RVPacker::CalcWGSCoord(){
	double Xsk = 0.;
	double Ysk = 0.;
	double Zsk = 0.;
	double Xpz = 0.;
	double Ypz = 0.;
	double Zpz = 0.;
	Geo2XYZ(Bsk, Lsk, Hsk, Xsk, Ysk, Zsk, IndSK);           // �������������� (��-42)    -> �������������  (��-42)
	XYZ2XYZ(Xsk, Ysk, Zsk, Xpz, Ypz, Zpz, SK42_PZ9002);     // �������������  (��-42)    -> �������������  (��-90.02)
	XYZ2XYZ(Xpz, Ypz, Zpz, Xwgs, Ywgs, Zwgs, PZ9002_WGS84); // �������������  (��-90.02) -> �������������  (WGS-84)
	XYZ2Geo(Xwgs, Ywgs, Zwgs, Bwgs, Lwgs, Hwgs, IndWGS84);  // �������������  (WGS-84)   -> �������������� (WGS-84)
	return 0;
}

//---------------------------------------------------------------------------
int TIM2RVPacker::ReCalcBx(){
	double SinLat = sin(Bwgs);
	double CosLat = cos(Bwgs);
	double SinLon = sin(Lwgs);
	double CosLon = cos(Lwgs);

	// ������� ������� ��� �����������, � �������� X - �� ������
	// Y - �� �����, Z ��������� � ����������.
	Bx[1] = - SinLon;
	Bx[2] = CosLon;
	Bx[3] = 0.;
	Bx[4] = - CosLon * SinLat;
	Bx[5] = - SinLon * SinLat;
	Bx[6] = CosLat;
	Bx[7] = CosLon * CosLat;
	Bx[8] = SinLon * CosLat;
	Bx[9] = SinLat;
	return 0;
}
//---------------------------------------------------------------------------
int TIM2RVPacker::SetData(){

	Params.X[0] = Xwgs;
	Params.X[1] = Ywgs;
	Params.X[2] = Zwgs;

	Params.V[0] = Bx[1] * V[0] + Bx[4] * V[1] + Bx[7] * V[2];
	Params.V[1] = Bx[2] * V[0] + Bx[5] * V[1] + Bx[8] * V[2];
	Params.V[2] = Bx[3] * V[0] + Bx[6] * V[1] + Bx[9] * V[2];

	Params.A[0] = Bx[1] * A[0] + Bx[4] * A[1] + Bx[7] * A[2];
	Params.A[1] = Bx[2] * A[0] + Bx[5] * A[1] + Bx[8] * A[2];
	Params.A[2] = Bx[3] * A[0] + Bx[6] * A[1] + Bx[9] * A[2];

	Params.Psi   = Psi;
	Params.Theta = Theta;
	Params.Gamma = Gamma;
	return 0;
}
